# Generated by Django 2.0.6 on 2018-09-03 04:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Scrum', '0002_auto_20180901_2151'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='scrumuser',
            name='age',
        ),
    ]
